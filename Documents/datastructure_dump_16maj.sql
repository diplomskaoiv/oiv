-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: spletna_aplikacija
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `email` varchar(255) NOT NULL,
  `ime` varchar(255) DEFAULT NULL,
  `geslo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('gemankapo@gmail.com','Miha','$2y$10$umD3n6BGYF2dAQCBMuYInOS8ngkceXVpPDW12tKFQ//ezjtTI.bIe');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dejavnost`
--

DROP TABLE IF EXISTS `dejavnost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dejavnost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  `opis` text,
  `ure` int(11) DEFAULT NULL,
  `izvajalec` varchar(255) DEFAULT NULL,
  `kraj` varchar(255) DEFAULT NULL,
  `cena` varchar(255) DEFAULT NULL,
  `datum` varchar(255) DEFAULT NULL,
  `realizirano` tinyint(1) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `kategorija_id` int(11) NOT NULL,
  `leto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dogodek_kategorija_leto1_idx` (`kategorija_id`,`leto`),
  CONSTRAINT `fk_dogodek_kategorija_leto1` FOREIGN KEY (`kategorija_id`, `leto`) REFERENCES `kategorija_leto` (`kategorija_id`, `leto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dejavnost`
--

LOCK TABLES `dejavnost` WRITE;
/*!40000 ALTER TABLE `dejavnost` DISABLE KEYS */;
INSERT INTO `dejavnost` VALUES (1,'Naziv 1','Opis tukaj',2,'Ime Izvajalca 1','Kraj 1','Cena dejavnosti','Datum izvedbe',0,1,5,2015),(2,'dejavnost_11','Opis spremenjene dejavnosti',2,'izvajalec 11','MS','3evre','jutri',1,1,2,2015),(3,'Naziv','Opis',0,'Izvajalec','Kraj','Cena','Datum',0,0,1,2015),(4,'dejavnost_11','Opis spremenjene dejavnosti',2,'izvajalec 11','MS','3evre','jutri',1,1,2,2015),(5,'ne','Opis',0,'Izvajalec','Kraj','Cena','Datum',0,0,1,2015),(6,'dejavnost 62','Opis spremenjene dejavnosti',2,'izvajalec 11','MS','3evre','jutri',0,1,2,2015),(7,'dejavnost_11','Opis spremenjene dejavnosti',2,'izvajalec 11','MS','3evre','jutri',1,1,2,2015);
/*!40000 ALTER TABLE `dejavnost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dejavnost_obvezno`
--

DROP TABLE IF EXISTS `dejavnost_obvezno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dejavnost_obvezno` (
  `dejavnost_id` int(11) NOT NULL,
  `oddelek` varchar(6) NOT NULL,
  PRIMARY KEY (`dejavnost_id`,`oddelek`),
  KEY `fk_dogodek_has_oddelek_oddelek1_idx` (`oddelek`),
  KEY `fk_dogodek_has_oddelek_dogodek1_idx` (`dejavnost_id`),
  CONSTRAINT `fk_dogodek_has_oddelek_dogodek1` FOREIGN KEY (`dejavnost_id`) REFERENCES `dejavnost` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dogodek_has_oddelek_oddelek1` FOREIGN KEY (`oddelek`) REFERENCES `oddelek` (`naziv`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dejavnost_obvezno`
--

LOCK TABLES `dejavnost_obvezno` WRITE;
/*!40000 ALTER TABLE `dejavnost_obvezno` DISABLE KEYS */;
INSERT INTO `dejavnost_obvezno` VALUES (1,'1.A'),(2,'2.B'),(3,'3.C'),(4,'2.C'),(4,'4.D'),(4,'4.Š'),(5,'4.Š'),(6,'2.D'),(6,'2.Š'),(7,'1.C'),(7,'1.D');
/*!40000 ALTER TABLE `dejavnost_obvezno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dijak`
--

DROP TABLE IF EXISTS `dijak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dijak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) DEFAULT NULL,
  `priimek` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8633850 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dijak`
--

LOCK TABLES `dijak` WRITE;
/*!40000 ALTER TABLE `dijak` DISABLE KEYS */;
INSERT INTO `dijak` VALUES (8562337,'Balažic','Blaž'),(8562347,'Čizmazija','Tamara'),(8562357,'Flisar','Maja'),(8562367,'Gider','Lea'),(8562377,'Ivanič','Aleš'),(8562387,'Jurkovič','Tilen'),(8562397,'Kolar','Tina'),(8562417,'Krajnc','Tjaša'),(8562427,'Krajnc','Vid Benjamin'),(8562437,'Krpič','Nina'),(8562447,'Kumin','Matjaž'),(8562457,'Lang','Lana'),(8562467,'Lang','Timotej'),(8562477,'Lebar','Mihael'),(8562487,'Malok','Tjaša'),(8562497,'Matuš','Živa'),(8562507,'Mlinarič','Hana'),(8562517,'Murnc','Luka'),(8562527,'Novak','Erik'),(8562537,'Perdigal','Nina'),(8562547,'Polanšček','Jakob'),(8562557,'Prša','Amadeja'),(8562567,'Rengeo','Iza'),(8562577,'Ritlop','Grega'),(8562587,'Šadl','Jure'),(8562597,'Špilak','Miha'),(8562607,'Vegi','Staš'),(8562617,'Zelko','Katarina'),(8562767,'Kovač','Maša'),(8633849,'Žižek','Ana');
/*!40000 ALTER TABLE `dijak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dijak_dejavnost`
--

DROP TABLE IF EXISTS `dijak_dejavnost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dijak_dejavnost` (
  `dijak_id` int(11) NOT NULL,
  `dejavnost_id` int(11) NOT NULL,
  PRIMARY KEY (`dijak_id`,`dejavnost_id`),
  KEY `fk_dogodek_has_dijak1_dijak1_idx` (`dijak_id`),
  KEY `fk_dogodek_has_dijak1_dogodek1_idx` (`dejavnost_id`),
  CONSTRAINT `fk_dogodek_has_dijak1_dijak1` FOREIGN KEY (`dijak_id`) REFERENCES `dijak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dogodek_has_dijak1_dogodek1` FOREIGN KEY (`dejavnost_id`) REFERENCES `dejavnost` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dijak_dejavnost`
--

LOCK TABLES `dijak_dejavnost` WRITE;
/*!40000 ALTER TABLE `dijak_dejavnost` DISABLE KEYS */;
INSERT INTO `dijak_dejavnost` VALUES (8562337,1),(8562347,1),(8562357,1),(8562367,1),(8562377,2),(8562387,2),(8562397,2),(8562417,3),(8562427,3),(8562437,4),(8562447,4),(8562457,4),(8562467,4),(8562477,4),(8562487,5),(8562497,5),(8562507,5),(8562517,5),(8562527,6),(8562537,6),(8562547,6),(8562557,6),(8562567,6),(8562577,6),(8562587,6),(8562597,6),(8562607,6),(8562617,6),(8562767,3),(8633849,6);
/*!40000 ALTER TABLE `dijak_dejavnost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dijak_oddelek`
--

DROP TABLE IF EXISTS `dijak_oddelek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dijak_oddelek` (
  `dijak_id` int(11) NOT NULL,
  `oddelek` varchar(6) NOT NULL,
  `leto` int(11) NOT NULL,
  PRIMARY KEY (`dijak_id`,`leto`),
  KEY `fk_oddelek_has_dijak_dijak1_idx` (`dijak_id`),
  KEY `fk_oddelek_has_dijak_oddelek1_idx` (`oddelek`),
  KEY `fk_oddelek_has_dijak_leto1_idx` (`leto`),
  CONSTRAINT `fk_oddelek_has_dijak_dijak1` FOREIGN KEY (`dijak_id`) REFERENCES `dijak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oddelek_has_dijak_leto1` FOREIGN KEY (`leto`) REFERENCES `leto` (`leto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oddelek_has_dijak_oddelek1` FOREIGN KEY (`oddelek`) REFERENCES `oddelek` (`naziv`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dijak_oddelek`
--

LOCK TABLES `dijak_oddelek` WRITE;
/*!40000 ALTER TABLE `dijak_oddelek` DISABLE KEYS */;
INSERT INTO `dijak_oddelek` VALUES (8562337,'1.A',2015),(8562347,'1.A',2015),(8562357,'1.B',2015),(8562367,'1.B',2015),(8562377,'1.C',2015),(8562387,'1.C',2015),(8562397,'1.D',2015),(8562767,'1.D',2015),(8562417,'1.Š',2015),(8562427,'1.Š',2015),(8562437,'2.A',2015),(8562447,'2.A',2015),(8562457,'2.B',2015),(8562467,'2.B',2015),(8562477,'2.C',2015),(8562487,'2.C',2015),(8562497,'2.D',2015),(8562617,'2.D',2015),(8562507,'2.Š',2015),(8562517,'3.A',2015),(8562527,'3.B',2015),(8562537,'3.C',2015),(8633849,'3.C',2015),(8562547,'3.D',2015),(8562557,'3.Š',2015),(8562567,'4.A',2015),(8562577,'4.B',2015),(8562587,'4.C',2015),(8562597,'4.D',2015),(8562607,'4.Š',2015);
/*!40000 ALTER TABLE `dijak_oddelek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1,'Državljanska kultura'),(2,'Knjižnično-informacijska znanja'),(3,'Kulturno umetniške vsebine'),(4,'Športni dnevi'),(5,'Zdravstvena vzgoja'),(6,'Vzgoja za družino, mir in nenasilje');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija_leto`
--

DROP TABLE IF EXISTS `kategorija_leto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija_leto` (
  `kategorija_id` int(11) NOT NULL,
  `leto` int(11) NOT NULL,
  PRIMARY KEY (`kategorija_id`,`leto`),
  KEY `fk_kategorija_has_leto_leto1_idx` (`leto`),
  KEY `fk_kategorija_has_leto_kategorija1_idx` (`kategorija_id`),
  CONSTRAINT `fk_kategorija_has_leto_kategorija1` FOREIGN KEY (`kategorija_id`) REFERENCES `kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kategorija_has_leto_leto1` FOREIGN KEY (`leto`) REFERENCES `leto` (`leto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija_leto`
--

LOCK TABLES `kategorija_leto` WRITE;
/*!40000 ALTER TABLE `kategorija_leto` DISABLE KEYS */;
INSERT INTO `kategorija_leto` VALUES (1,2014),(2,2014),(4,2014),(6,2014),(1,2015),(2,2015),(3,2015),(4,2015),(5,2015),(6,2015);
/*!40000 ALTER TABLE `kategorija_leto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leto`
--

DROP TABLE IF EXISTS `leto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leto` (
  `leto` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`leto`)
) ENGINE=InnoDB AUTO_INCREMENT=2016 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leto`
--

LOCK TABLES `leto` WRITE;
/*!40000 ALTER TABLE `leto` DISABLE KEYS */;
INSERT INTO `leto` VALUES (2014,'2014/15'),(2015,'2015/16');
/*!40000 ALTER TABLE `leto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oddelek`
--

DROP TABLE IF EXISTS `oddelek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oddelek` (
  `naziv` varchar(6) NOT NULL,
  `letnik` int(11) DEFAULT NULL,
  PRIMARY KEY (`naziv`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oddelek`
--

LOCK TABLES `oddelek` WRITE;
/*!40000 ALTER TABLE `oddelek` DISABLE KEYS */;
INSERT INTO `oddelek` VALUES ('1.A',1),('1.B',1),('1.C',1),('1.D',1),('1.Š',1),('2.A',2),('2.B',2),('2.C',2),('2.D',2),('2.Š',2),('3.A',3),('3.B',3),('3.C',3),('3.D',3),('3.Š',3),('4.A',4),('4.B',4),('4.C',4),('4.D',4),('4.Š',4);
/*!40000 ALTER TABLE `oddelek` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-16 11:45:30
