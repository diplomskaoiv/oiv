$(document).ready(function() {

  $('#dejavnosti_prijave').multiselect({
      columns: 5,
      selectGroup: true,
      minHeight: 500,
      texts: {
          placeholder: 'Izberi dijake'
      }
  });

});
