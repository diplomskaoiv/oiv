<main>
  
<div class="message_box warning_box"><?= (isset($data['warning_msg']))? $data['warning_msg'] : "" ?></div>
<div class="message_box info_box"><?= (isset($data['info_msg']))? $data['info_msg'] : "" ?></div>
<div class="message_box error_box"><?= (isset($data['error_msg']))? $data['error_msg'] : "" ?></div>
<div class="message_box success_box"><?= (isset($data['success_msg']))? $data['success_msg'] : "" ?></div>
