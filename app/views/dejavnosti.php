<h1><?= $data['view_title'] ?></h1>
<?php foreach($data['dejavnosti'] as $result): ?>
  <h2><?= $result['naziv'] ?></h1>
  <p><?= $result['opis'] ?></p>
  <p><?= $result['kraj'] ?>, <?= $result['datum'] ?></p>
  <p><b>Izvajalec:</b> <?= $result['izvajalec'] ?></p>
  <p><b>Priznane ure:</b> <?= "ure: ".$result['ure'] ?></p>
  <?= ($result['cena'] != "") ? "<p><b>Cena:</b> ".$result['cena']."</p>" : "" ?>
  <p><b>Kategorija:</b> <?= $result['kategorija'] ?></p>
  <ul>
    <li><?= ($result['realizirano'] == 1)? "Realizirano" : "Nerealizirano" ?></li>
    <li><?= ($result['tip'] == 0)? "Nedoločena" : "Določena" ?></li>
  </ul>
  <a class="submit_buttons" id="submit_delete" href="<?= URL."dejavnosti/change/".$result['id'] ?>">Uredi</a>
  <div style="clear:both;"></div>
  <hr />
<?php endforeach ?>
