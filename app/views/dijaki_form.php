<h1><?= $data['view_title'] ?></h1>
<form action="<?= $data['form_action'] ?>" method="post">
<table>
  <tr><td class="table_label">ID</td><td><input type="text" name="id" value="<?= (isset($data['id']))? $data['id'] : "" ?>"></td><td class="table_description">"ID učenca" iz eAssistent</td></tr>
  <tr><td class="table_label">Ime</td><td><input type="text" name="ime" value="<?= (isset($data['ime']))? $data['ime'] : "" ?>"></td><td class="table_description"></td></tr>
  <tr><td class="table_label">Priimek</td><td><input type="text" name="priimek" value="<?= (isset($data['priimek']))? $data['priimek'] : "" ?>"></td><td class="table_description"></td></tr>
  <tr><td class="table_label">Oddelek</td>
    <td>
      <select name="oddelek">
        <?php foreach($data['oddelki'] AS $oddelek){ ?>
          <option value="<?= $oddelek ?>"><?= $oddelek ?></option>
        <?php } ?>
      </select>
    </td>
    <td class="table_description"></td>
  </tr>
</table>
<?php require_once "app/views/submit_buttons.php"; ?>
</form>
