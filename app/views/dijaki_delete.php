<h1><?= $data['view_title'] ?></h1>
<p><?= $data['ime']." ".$data['priimek'] ?></p>
<p>Oddelek: <?= $data['oddelek'] ?></p>
<form action="<?= $data['form_action'] ?>" method="post">
<div id="submit_wrap">
  <input class="submit_buttons" id="submit_delete" name="delete" type="submit" value="Izbriši">
  <a class="submit_buttons" id="submit_cancel" href="<?= $data['preklici'] ?>">Prekliči</a>
  <div style="clear:both;"></div>
</div>
</form>
