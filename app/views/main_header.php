<header>

  <div id="site_title">
    <a href="<?= URL ?>">OIV GMS</a>
  </div>

  <?php if(App::loggedUser()){ ?>

  <form id="izbira_leta" action="<?= URL_HOSTONLY . $_SERVER['REQUEST_URI'] ?>" method="POST">
    <select id="izbrano_leto" name="izbrano_leto">
      <?php foreach($data['leta'] as $option): ?>
        <option value="<?= $option['leto'] ?>" <?= ($option['leto'] == $_SESSION['leto'])? "selected" : "" ?>><?= $option['naziv'] ?></option>
      <?php endforeach ?>
    </select>
    <a id="addYear" href="<?= URL; ?>addyear">Dodaj leto</a>
  </form>


  <hr />

  <nav>
    <ul>
      <li><a href="<?= URL; ?>home" <?= ($GLOBALS['controller']=="home")? "class=\"selected\"" : ""?>>Domov</a></li>
      <!-- Dejavnosti -->
      <li><a href="<?= URL; ?>dejavnosti" <?= ($GLOBALS['controller']=="dejavnosti")? "class=\"selected\"" : ""?>>Dejavnosti</a></li>
      <li style="display:<?= ($GLOBALS['controller']=="dejavnosti")? "show" : "none" ?>">
        <ul>
          <li><a href="<?= URL; ?>dejavnosti/add">Dodaj novo</a></li>
        </ul>
      </li>
      <!-- Kategorije -->
      <li><a href="<?= URL; ?>kategorije" <?= ($GLOBALS['controller']=="kategorije")? "class=\"selected\"" : ""?>>Kategorije</a></li>
      <li style="display:<?= ($GLOBALS['controller']=="kategorije")? "show" : "none" ?>">
        <ul>
          <li><a href="<?= URL; ?>kategorije/add">Dodaj novo</a></li>
        </ul>
      </li>
      <!-- Dijaki -->
      <li><a href="<?= URL; ?>dijaki" <?= ($GLOBALS['controller']=="dijaki")? "class=\"selected\"" : ""?>>Dijaki</a></li>
      <li style="display:<?= ($GLOBALS['controller']=="dijaki")? "show" : "none" ?>">
        <ul>
          <li><a href="<?= URL; ?>dijaki/add">Dodaj novo</a></li>
        </ul>
      </li>
      <!-- Oddelki -->
      <li><a href="<?= URL; ?>oddelki" <?= ($GLOBALS['controller']=="oddelki")? "class=\"selected\"" : ""?>>Oddelki</a></li>
    </ul>
  </nav>

  <hr />

  <ul id="user_options">
    <li id="user_name"><?= $_SESSION['user'] ?></li>
    <li>
      <ul>
        <li><a href="<?= URL; ?>odjava">Odjava</a></li>
      </ul>
    </li>
  </ul>

  <?php }else{ ?>

    <a class="submit_buttons" id="submit_login" href="<?= URL."prijava" ?>">Prijava</a>

  <?php } ?>

</header>
