<?php

class m_prijava extends Model{

  public function adminLogin($email){

    $connection = $this->connect();

    // SQL statement
    $sql="SELECT ime,geslo FROM admin WHERE email=?";

    // Prepare statement
    $stmt = $connection->prepare($sql);
    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('s',$email);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($name,$pwd);
    while($stmt->fetch()){
      return array('name' => $name, 'pwd' => $pwd);
    }

    // Clear memory
    $stmt->close();

  }

}
