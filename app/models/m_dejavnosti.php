<?php

class m_dejavnosti extends Model{

  public function index($leto){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dejavnost.id,dejavnost.naziv,dejavnost.opis,dejavnost.ure,dejavnost.izvajalec,dejavnost.kraj,dejavnost.cena,dejavnost.datum,dejavnost.realizirano,dejavnost.tip,kategorija.naziv,leto.naziv FROM dejavnost LEFT JOIN kategorija_leto ON (dejavnost.kategorija_id = kategorija_leto.kategorija_id AND dejavnost.leto = kategorija_leto.leto) LEFT JOIN kategorija ON (kategorija_leto.kategorija_id = kategorija.id)
    LEFT JOIN leto ON (dejavnost.leto = leto.leto) WHERE dejavnost.leto = ?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$leto);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($id,$naziv,$opis,$ure,$izvajalec,$kraj,$cena,$datum,$realizirano,$tip,$kategorija,$leto);

    // create data array
    $data = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($data,["id" => $id,"naziv" => $naziv,"opis" => $opis,"ure" => $ure,"izvajalec" => $izvajalec,"kraj" => $kraj,"cena" => $cena,"datum" => $datum,"realizirano" => $realizirano,"tip" => $tip,"kategorija" => $kategorija,"leto" => $leto]);
    }

    // Clear memory
    $stmt->close();

    // return data
    return $data;

  }

  public function changeDejavnost(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("UPDATE dejavnost SET naziv=?,opis=?,ure=?,izvajalec=?,kraj=?,cena=?,datum=?,realizirano=?,tip=?,kategorija_id=? WHERE id=?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('ssissssiiii',$data['naziv'],$data['opis'],$data['ure'],$data['izvajalec'],$data['kraj'],$data['cena'],$data['datum'],$data['realizirano'],$data['tip'],$data['kategorija'],$data['id']);

    // Execute statement
    $stmt->execute();

    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // new connection -- delete obvezni oddelki
    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("DELETE FROM dejavnost_obvezno WHERE dejavnost_id = ?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    if($stmt->affected_rows > 0)
      $return = 1;

    // Clear memory
    $stmt->close();

    if($data['tip'] == 1){

      if(isset($data['oddelki'])){

        foreach($data['oddelki'] as $oddelek){

          $connection = $this->connect();

          // Prepare statement
          $stmt = $connection->prepare("INSERT INTO dejavnost_obvezno (dejavnost_id,oddelek) VALUES (?,?)");

          // Return any errors
          if($stmt === false){
            trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
          }

          // Bind parameters; s = string, i = integer, d = double,  b = blob
          $stmt->bind_param('is',$data['id'],$oddelek);

          // Execute statement
          $stmt->execute();

          $return = $stmt->affected_rows;

          // Clear memory
          $stmt->close();

        }

      }

    }

    // new connection -- delete prijavljeni dijaki
    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("DELETE FROM dijak_dejavnost WHERE dejavnost_id = ?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    if($stmt->affected_rows > 0)
      $return = 1;

    // Clear memory
    $stmt->close();

    if($data['dejavnosti_prijave'] != false){

      foreach($data['dejavnosti_prijave'] as $dijak_id){

        $connection = $this->connect();

        // Prepare statement
        $stmt = $connection->prepare("INSERT INTO dijak_dejavnost (dijak_id,dejavnost_id) VALUES (?,?)");

        // Return any errors
        if($stmt === false){
          trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
        }

        // Bind parameters; s = string, i = integer, d = double,  b = blob
        $stmt->bind_param('ii',$dijak_id,$data['id']);

        // Execute statement
        $stmt->execute();

        $return = $stmt->affected_rows;

        // Clear memory
        $stmt->close();

      }

    }

    return $return;

  }

  public function returnDejavnost(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT naziv,opis,ure,izvajalec,kraj,cena,datum,realizirano,tip,kategorija_id,leto FROM dejavnost WHERE id=?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($naziv,$opis,$ure,$izvajalec,$kraj,$cena,$datum,$realizirano,$tip,$kategorija,$leto);

    // push data into array
    while ($stmt->fetch()) {
      $data['naziv'] = $naziv;
      $data['opis'] = $opis;
      $data['ure'] = $ure;
      $data['izvajalec'] = $izvajalec;
      $data['kraj'] = $kraj;
      $data['cena'] = $cena;
      $data['datum'] = $datum;
      $data['realizirano'] = $realizirano;
      $data['tip'] = $tip;
      $data['kategorija'] = $kategorija;
      $data['leto'] = $leto;
    }

    // Clear memory
    $stmt->close();

  }

  public function returnKategorije($leto){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT kategorija.id,kategorija.naziv FROM kategorija LEFT JOIN kategorija_leto ON (kategorija.id  = kategorija_leto.kategorija_id) WHERE kategorija_leto.leto=?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$leto);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($id,$naziv);

    // set data array
    $result = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($result,["id" => $id,"naziv" => $naziv]);
    }

    // Clear memory
    $stmt->close();

    // return data
    return $result;

  }

  public function deleteDejavnost($id,$leto){

    // Establish DB connection
    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("DELETE FROM dejavnost WHERE id=? AND leto=?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('ii',$id,$leto);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

  public function addDejavnost($data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO dejavnost (naziv,opis,ure,izvajalec,kraj,cena,datum,realizirano,tip,kategorija_id,leto) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('ssissssiiii',$data['naziv'],$data['opis'],$data['ure'],$data['izvajalec'],$data['kraj'],$data['cena'],$data['datum'],$data['realizirano'],$data['tip'],$data['kategorija'],$data['leto']);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;
    $id = $stmt->insert_id;

    // Clear memory
    $stmt->close();

    if($data['tip'] == 1){

      if(isset($data['oddelki'])){

        foreach($data['oddelki'] as $oddelek){

          $connection = $this->connect();

          // Prepare statement
          $stmt = $connection->prepare("INSERT INTO dejavnost_obvezno (dejavnost_id,oddelek) VALUES (?,?)");

          // Return any errors
          if($stmt === false){
            trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
          }

          // Bind parameters; s = string, i = integer, d = double,  b = blob
          $stmt->bind_param('is',$id,$oddelek);

          // Execute statement
          $stmt->execute();

          // Clear memory
          $stmt->close();

        }

      }

    }

    // return affected rows
    return $return;

  }

  public function returnOddelki(){

    // Establish DB connection
    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT naziv,letnik FROM oddelek");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($naziv,$letnik);

    // Set result array
    $result = [1 => [],2 => [],3 => [],4 => []];

    // push data into array
    while ($stmt->fetch()){

      array_push($result[$letnik],$naziv);

    }

    // Clear memory
    $stmt->close();

    // return affected rows
    return $result;

  }

  public function returnOddelkiObvezno(&$data){

    // Establish DB connection
    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT oddelek FROM dejavnost_obvezno WHERE dejavnost_id = ?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($oddelek);

    // push data into array
    while ($stmt->fetch()){

      $data['oddelki'][$oddelek] = $oddelek;

    }

    // Clear memory
    $stmt->close();

  }

  Public function returnDijaki(){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dijak.id,dijak.ime,dijak.priimek,dijak_oddelek.oddelek FROM dijak INNER JOIN dijak_oddelek ON (dijak.id = dijak_oddelek.dijak_id) WHERE leto = ? ORDER BY dijak_oddelek.oddelek ASC");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$_SESSION['leto']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($id,$ime,$priimek,$oddelek);

    // push data into array
    while ($stmt->fetch()) {
      $data[$id] = ['ime' => $ime,'priimek' => $priimek,'oddelek' => $oddelek];
    }

    // return data
    return $data;

    // Clear memory
    $stmt->close();

  }

  public function returnDejavnostiPrijave(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dijak_id FROM dijak_dejavnost WHERE dejavnost_id = ?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($dijak_id);

    // push data into array
    while ($stmt->fetch()) {
      $data['dejavnosti_prijave'][$dijak_id]['selected'] = true;
    }

    // Clear memory
    $stmt->close();

  }

}
