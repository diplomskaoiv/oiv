<?php

class home extends Controller{

  public function index(){

    $model = $this->model('m_dejavnosti');
    $data['dejavnosti'] = $model->index(Model::yearCurrent());

    $data['view_title'] = "Pregled letošnjih dejavnosti";
    $this->view(get_class($this),$data);

  }

}
