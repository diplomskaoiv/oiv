<?php

class kategorije extends Controller{

  public function index(){

    $model = $this->model('m_'.get_class($this));
    $data['kategorije'] = $model->index($_SESSION['leto']);

    $data['view_title'] = "Kategorije";
    $this->view(get_class($this),$data);

  }

  public function change($id){

    $data['id'] = $this->filterIntInput($id);

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['submit'])){

      $data['naziv'] = $_POST['naziv'];

      if($model->changeKategorija($data['id'],$data['naziv']) > 0)
        $data['success_msg'] = "Kategorija uspešno posodobljena!";
      else
        $data['success_msg'] = "Kategorija neuspešno posodobljena!";

    }else{

      $model->returnKategorija($data);

    }

    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$data['id'];
    $data['controller'] = __CLASS__;

    $data['view_title'] = "Urejanje kategorije";
    $this->view(get_class($this).'_form',$data);

  }

  public function delete($id){

    $data['id'] = $this->filterIntInput($id);

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['delete'])){

      if($model->deleteKategorija($data['id']) > 0)
        $data['success_msg'] = "Kategorija uspešno izbrisana!";
      else
        $data['success_msg'] = "Kategorija neuspešno izbrisana!";

      $this->view("",$data);

    }else{

    $model->returnKategorija($data);

    $data['preklici'] = URL.__CLASS__."/change/".$data['id'];
    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$data['id'];
    $data['warning_msg'] = "Potrebna potrditev izbrisa!";


    $data['view_title'] = "Izbris kategorije";
    $this->view(get_class($this).'_delete',$data);

    }

  }

  public function add(){

    if(isset($_POST['submit'])){

      $data['naziv'] = $_POST['naziv'];

      $model = $this->model('m_'.get_class($this));

      if($model->addKategorija($data['naziv'],$_SESSION['leto']) > 0)
        $data['success_msg'] = "Kategorija uspešno dodana!";
      else
        $data['error_msg'] = "Kategorija neuspešno dodana!";

      $this->view("",$data);

    }else{

      $data['form_action'] = URL.__CLASS__."/".__FUNCTION__;
      $data['controller'] = __CLASS__;

      $data['view_title'] = "Dodaja kategorije";
      $this->view(get_class($this).'_form',$data);

    }

  }

}
